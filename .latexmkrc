$pdflatex = 'pdflatex -interaction nonstopmode --shell-escape %O %S';
$recorder = 1;
$hash_calc_ignore_pattern{'pdf'} = '.*';

add_cus_dep('glo', 'gls', 0, 'run_makeglossaries');
add_cus_dep('acn', 'acr', 0, 'run_makeglossaries');

sub run_makeglossaries {
  if ( $silent ) {
    system "makeglossaries -q '$_[0]'";
  }
  else {
    system "makeglossaries '$_[0]'";
  };
}
push @generated_exts, 'glo', 'gls', 'glg';
push @generated_exts, 'acn', 'acr', 'alg';
$clean_ext .= ' %R.ist %R.xdy';

add_cus_dep('dot', 'pdf', 0, 'run_dot');
sub run_dot {
  if ( $silent ) {
    system "dot -Gcharset=UTF-8 -Tpdf -o'$_[0].pdf' '$_[0].dot'";
  }
  else {
    system "dot -v -Gcharset=UTF-8 -Tpdf -o'$_[0].pdf' '$_[0].dot'";
  };
  &cus_dep_require_primary_run;
}

add_cus_dep('puml', 'pdf', 0, 'run_plantuml');
sub run_plantuml {
  if ( $silent ) {
    system "plantuml -tpdf -quiet '$_[0]'.puml";
  }
  else {
    system "plantuml -tpdf -v '$_[0]'.puml";
  }
  &cus_dep_require_primary_run;
}

add_cus_dep('nwdiag', 'pdf', 0, 'run_nwdiag');
sub run_nwdiag {
  if ( $silent ) {
    system "nwdiag3 -Tpdf  '$_[0]'.nwdiag";
  }
  else {
    system "nwdiag3 -Tpdf --debug '$_[0]'.nwdiag";
  }
  &cus_dep_require_primary_run;
}
