UPDATE public.ir_ui_view SET
arch_db = '<?xml version="1.0"?>
<tree string="Contacts">
  <field name="display_name" string="Name"/>
  <field name="function" invisible="1"/>
  <field name="phone" class="o_force_ltr" optional="show"/>
  <field name="mobile" class="o_force_ltr" optional="show"/>
  <field name="email" optional="show"/>
  <field name="company_id" groups="base.group_multi_company"/>
  <field name="city" optional="hide"/>
  <field name="state_id" optional="hide"/>
  <field name="country_id" optional="hide"/>
  <field name="vat" optional="hide"/>
  <field name="user_id" invisible="1"/>
  <field name="is_company" invisible="1"/>
  <field name="parent_id" invisible="1"/>
  <field name="active" invisible="1"/>
</tree>
'::text
WHERE name = 'res.partner.tree';
